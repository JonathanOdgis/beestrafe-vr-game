﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = new Vector3 (PlayerController.Instance.transform.position.x, this.transform.position.y, PlayerController.Instance.transform.position.z);
	}
}
