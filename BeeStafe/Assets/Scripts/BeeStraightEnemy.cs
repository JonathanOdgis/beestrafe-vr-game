﻿using UnityEngine;
using System.Collections;

public class BeeStraightEnemy : MonoBehaviour {
	Rigidbody rgb;
	public float speed = 20;
	// Use this for initialization
	void Start () {
		rgb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += transform.forward * speed * Time.deltaTime;
	}
}
