﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public static PlayerController Instance;
	private Rigidbody rgb;
	public Camera cam;
	[SerializeField] public LayerMask WhatIsGround;
	public float moveSpeed = 20;
	public float speed = 400;
	public float strafeCooldown;
	public bool canStrafe;
	// Use this for initialization
	void Start () {
		Instance = this;
		rgb = GetComponent<Rigidbody> ();
		//cam = GetComponentInChildren<Camera> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, cam.transform.eulerAngles.y, this.transform.eulerAngles.z);

		if (strafeCooldown == 0 && canStrafe) {
			if (Input.GetKeyDown(KeyCode.A) ||  (cam.transform.eulerAngles.z < 80 && cam.transform.eulerAngles.z > 10))
			{			
				rgb.velocity = -transform.right * speed * Time.deltaTime;//transform.position += -transform.right * speed * Time.deltaTime;
				strafeCooldown = .3f;
			}

			if (Input.GetKeyDown(KeyCode.D) ||   (cam.transform.eulerAngles.z < 348 && cam.transform.eulerAngles.z > 275))
			{
				rgb.velocity = transform.right * speed * Time.deltaTime;//transform.position += transform.right * speed * Time.deltaTime;
				strafeCooldown = .3f;
			}
		} 

		//TODO if you are facing somewhere with the camera, make the strafe relative to the cameras angle and then rotate the body that way 


		//Cam ray
		Vector3 fwd = cam.transform.TransformDirection(Vector3.forward);
		RaycastHit hit;

		if (Physics.Raycast (transform.position, fwd, out hit, 100, WhatIsGround)) {
			this.transform.position = Vector3.MoveTowards (this.transform.position, new Vector3 (hit.point.x, this.transform.position.y, hit.point.z), moveSpeed * Time.deltaTime);
			this.transform.eulerAngles = new Vector3 (this.transform.eulerAngles.x, hit.transform.eulerAngles.y, this.transform.eulerAngles.z);
		}
			
		if (strafeCooldown > 0) {
			canStrafe = false;
			strafeCooldown -= .1f;
		}
		if (strafeCooldown <= 0) {
			canStrafe = true;
			strafeCooldown = 0;
		}
	}
}
