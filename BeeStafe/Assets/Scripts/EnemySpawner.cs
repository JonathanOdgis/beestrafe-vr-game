﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {
	public GameObject enemy;
	public float spawnDelay = 5f;
	// Use this for initialization
	void Start () {
		StartCoroutine (SpawnEnemies ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator SpawnEnemies()
	{
		while (true) {
			GameObject en = Instantiate (enemy.gameObject, this.transform.position, this.transform.rotation) as GameObject;
			yield return new WaitForSeconds (spawnDelay);
		}
	}
}
