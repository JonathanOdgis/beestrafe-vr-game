# BEESTRAFE #

BeeStrafe is an experimental VR experience for Google Cardboard. The game is an arena-based first person game where the player must dodge bees as they come buzzing towards you. I'm sure players will "hive" a good time.